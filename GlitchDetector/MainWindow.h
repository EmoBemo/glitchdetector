#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore\QScopedPointer>
#include <QtWidgets/QMainWindow>
#include "ui_mainwindow.h"

class GlitchDetector;
class QDoubleSpinBox;
	
class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void load();
	void about();
	void browseGlitches();
	void resetGlitchView();
	void updateGlitchView();

protected:
	// Reimplemented
	void dragEnterEvent( QDragEnterEvent * event);

	// Reimplemented
	void dropEvent( QDropEvent * event);

	// Reimplemented
	void closeEvent( QCloseEvent * event);

private:
	Ui::MainWindowClass ui;
	GlitchDetector * core;
	QDoubleSpinBox * spinThreshold;
	QScopedPointer<QTableView> glitchTableView;
};

#endif // MAINWINDOW_H
