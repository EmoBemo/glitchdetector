#include "MainWindow.h"
#include <QtCore\QSettings>
#include <QtCore\QDate>
#include <QtCore\QMimeData>
#include <QtCore\QStandardPaths>
#include <QtGui\QDragEnterEvent>
#include <QtGui\QDropEvent>
#include <QtWidgets\QFileDialog>
#include <QtWidgets\QDoubleSpinBox>
#include <QtWidgets\QLabel>
#include <QtWidgets\QMessageBox>
#include <QtWidgets\QTableView>
#include "glitchdetector.h"
#include "progressdelegate.h"
#include "timedelegate.h"

#define SUPPORTED_FILES QStringLiteral("wav;flac;aiff;au;snd;voc;w64;pvf;caf;sd2;iff;svx;ogg")

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, glitchTableView(0)
{
	ui.setupUi(this);
	setCentralWidget(ui.tableView);
	ui.action_About->setText(tr("&About %1").arg(qApp->applicationName()));

	QSettings settings;
	core = new GlitchDetector(this);

	// File menu
	connect(ui.action_Load, SIGNAL(triggered()), this, SLOT(load()));
	connect(ui.action_Clear, SIGNAL(triggered()), core, SLOT(reset()));
	connect(ui.actionExit, SIGNAL(triggered()), this, SLOT(close()));

	// Process menu
	connect(ui.action_Detect, SIGNAL(triggered()), core, SLOT(rescan()));
	connect(ui.action_Abort, SIGNAL(triggered()), core, SLOT(abort()));
	connect(ui.actionBrowse_glitches, SIGNAL(triggered()), this, SLOT(browseGlitches()));

	// Help menu
	connect(ui.action_About, SIGNAL(triggered()), this, SLOT(about()));
	connect(ui.action_About_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
	
	ui.tableView->setModel(core->taskModel());
	ui.tableView->setItemDelegateForColumn(GlitchDetector::Progress, new ProgressDelegate(ui.tableView));
	connect(ui.tableView, SIGNAL(doubleClicked(QModelIndex)),
			this, SLOT(browseGlitches()));
	connect(ui.tableView->selectionModel(), SIGNAL(currentChanged(const QModelIndex&, const QModelIndex&)),
			this, SLOT(updateGlitchView()));

	ui.mainToolBar->addAction(ui.action_Load);
	ui.mainToolBar->addSeparator();
	ui.mainToolBar->addAction(ui.action_Detect);
	ui.mainToolBar->addAction(ui.action_Abort);
	ui.mainToolBar->addAction(ui.actionBrowse_glitches);
	ui.mainToolBar->addSeparator();
	ui.mainToolBar->addWidget(new QLabel("Threshold", ui.mainToolBar));

	spinThreshold = new QDoubleSpinBox(ui.mainToolBar);
	connect(spinThreshold, SIGNAL(valueChanged(double)), core, SLOT(setThreshold(double)));
	spinThreshold->setMaximum(1.0);
	spinThreshold->setSingleStep(0.05);
	spinThreshold->setValue(settings.value("Threshold", 0.1).toDouble());
	ui.mainToolBar->addWidget(spinThreshold);
}

MainWindow::~MainWindow()
{
	QSettings settings;
	settings.setValue("Threshold", spinThreshold->value());
}

void MainWindow::load()
{
	QSettings settings;
	static const QStringList &standardMusicLocations = QStandardPaths::standardLocations(QStandardPaths::MusicLocation);
	static const QString standardAudioPath = standardMusicLocations.isEmpty() ? QString() : standardMusicLocations.front();
	const QString &sDir = settings.value("LastOpenFolder", standardAudioPath).toString();
	
	static const QString &sFilter = 
		QString("Audio files (%1)").arg(QStringLiteral("*.") + SUPPORTED_FILES.split(QChar(';')).join(QStringLiteral(" *.")));

	QStringList listFiles = QFileDialog::getOpenFileNames(this, tr("Open"), sDir, sFilter);
	if (listFiles.isEmpty())
		return;

	settings.setValue("LastOpenFolder", listFiles.front());

	foreach (const QString &file, listFiles)
	{
		GlitchDetector::TaskID id = core->addFile(file);
		core->startTask(id);
	}
}

void MainWindow::about()
{
	QMessageBox msg(QMessageBox::NoIcon, tr("About"),
					tr("<p><b>%1 %2</b></p><br/><br/><p>\
Copyright %4 %3. All rights reserved.\
The program is provided AS IS with NO WARRANTY OF ANY KIND, \
INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</p>").
					arg(qAppName()).
					arg(qApp->applicationVersion()).
					arg(qApp->organizationName()).
					arg(QDate::currentDate().year()), QMessageBox::Ok, this);
	msg.setIconPixmap(QPixmap(":/GlitchDetector/resources/sine_wave_256x256.png"));
	msg.exec();
}

void MainWindow::browseGlitches()
{
	const QModelIndex &current = ui.tableView->currentIndex();
	if (!current.isValid())
		return;

	GlitchDetector::TaskID taskID = core->taskID(current.row());
	if (taskID < 0)
		return;

	QAbstractItemModel * glitchModel = core->glitchModel(taskID);
	if (!glitchModel)
		return;

	// Lazy create view
	if (!glitchTableView)
	{
		glitchTableView.reset(new QTableView);
		glitchTableView->setItemDelegateForColumn(1, new TimeDelegate(glitchTableView.data()));
		glitchTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
		glitchTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
		glitchTableView->horizontalHeader()->setStretchLastSection(true);
		glitchTableView->resize(400, 400);
	}

	glitchTableView->setModel(glitchModel);
	glitchTableView->show();
	glitchTableView->raise();
	connect(glitchModel, SIGNAL(destroyed(QObject*)), this, SLOT(resetGlitchView()));
}

void MainWindow::resetGlitchView()
{
	if (!glitchTableView.isNull())
		glitchTableView->setModel(0);
}

void MainWindow::updateGlitchView()
{
	if (!glitchTableView.isNull() && glitchTableView->isVisible() &&
			ui.tableView->currentIndex().isValid())
		browseGlitches();
}

void MainWindow::dragEnterEvent( QDragEnterEvent * event)
{
	const QMimeData * pData = event->mimeData();
	if (pData)
	{
		const QList<QUrl> &listFiles = pData->urls();
		// Check if the type of at least one of the files dragged is supported.
		foreach (QUrl url, listFiles)
		{
			const QString &sSuffix = QFileInfo(url.toLocalFile()).suffix();
			if (SUPPORTED_FILES.contains(sSuffix, Qt::CaseInsensitive))				
			{
				event->acceptProposedAction();
				break;
			}
		}		
	}	
}

void MainWindow::dropEvent( QDropEvent * event)
{
	const QMimeData * pData = event->mimeData();
	if (pData)
	{
		const QList<QUrl> &listFiles = pData->urls();
		foreach (QUrl url, listFiles)
		{
			QFileInfo fInfo(url.toLocalFile());
			if (!fInfo.isFile())
				return;

			const QString &sPath = fInfo.filePath();
			const QString &sSuffix = fInfo.suffix();
			if (SUPPORTED_FILES.contains(sSuffix, Qt::CaseInsensitive))
			{
				GlitchDetector::TaskID id = core->addFile(sPath);
				core->startTask(id);
			}	
		}
	}
}

void MainWindow::closeEvent( QCloseEvent * )
{
	if (glitchTableView && glitchTableView->isVisible())
		glitchTableView->close();
}
