#ifndef TASK_H
#define TASK_H

#include <QtCore\QSharedPointer>

class Task
{
public:
	enum Status {
		Pending,
		Processing,
		Finished,
		Error,
		Aborted,
	};
public:
	Task();
	virtual ~Task();

	void setStatus(Status status) { m_status = status; }
	Status status() const { return m_status; }

	int id() const { return m_id; }

	int progress() const { return m_progress; }

protected:
	void setProgress(int progress) { m_progress = progress; }

private:
	Status m_status;
	int m_id;
	int m_progress;
};

typedef QSharedPointer<Task> SPTask;

#endif // TASK_H
