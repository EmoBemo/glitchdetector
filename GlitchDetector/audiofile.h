#ifndef AUDIOFILE_H
#define AUDIOFILE_H

#include <QtCore\QString>
#include <QtCore\QIODevice>

class AudioFilePrivate;

class AudioFile
{
public:
	AudioFile(const QString &name = QString());
	~AudioFile();

	void setPath(const QString &path);
	bool open(QIODevice::OpenMode mode);
	bool isOpen() const;
	bool exists();
	void close();

	int read(float * buf, int frames);
	int read(int * buf, int frames);
	int read(short * buf, int frames);

	qint64 frames() const;

	qint64 audioLength() const;

	int samplerate() const;

	int channels() const;

private:
	AudioFilePrivate *p;
};

#endif // AUDIOFILE_H
