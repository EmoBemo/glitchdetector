#ifndef FILETASK_H
#define FILETASK_H

#include "task.h"
#include <QtCore\QScopedPointer>
#include <QtCore\QString>

class QAbstractItemModel;

class FileTask : public Task
{
public:
	enum GlitchColumn {
		FrameNum,
		Time_ms,
		Diff,
		GlitchColumn_End
	};
public:
	FileTask(const QString &path);

	void addGlitch(const int & frame, const int &time_ms, const float &diff);

	void clearGlitchModel();

	QString path() const { return m_path; }

	QAbstractItemModel * glitchModel() const;

private:
	QScopedPointer<QAbstractItemModel> m_glitchModel;
	const QString m_path;
};

#endif // FILETASK_H
