#include "filetask.h"
#include <QtGui\QStandardItemModel>
#include <QtWidgets\QApplication>

FileTask::FileTask( const QString &path )
	: Task()
	, m_glitchModel(new QStandardItemModel(0, GlitchColumn_End))
	, m_path(path)
{
	m_glitchModel->setHeaderData(FrameNum, Qt::Horizontal, QApplication::tr("Frame"));
	m_glitchModel->setHeaderData(Time_ms, Qt::Horizontal, QApplication::tr("Time"));
	m_glitchModel->setHeaderData(Diff, Qt::Horizontal, QApplication::tr("Diff"));
}

void FileTask::addGlitch( const int & frame, const int &time_ms, const float &diff )
{
	const int row = m_glitchModel->rowCount();
	m_glitchModel->insertRow(row);
	m_glitchModel->setData(m_glitchModel->index(row, FrameNum), frame);
	m_glitchModel->setData(m_glitchModel->index(row, Time_ms), time_ms);
	m_glitchModel->setData(m_glitchModel->index(row, Diff), diff);
}

void FileTask::clearGlitchModel()
{
	m_glitchModel->removeRows(0, m_glitchModel->rowCount());
}

QAbstractItemModel *FileTask::glitchModel() const
{
	return m_glitchModel.data();
}
