#ifndef GLITCHDETECTOR_H
#define GLITCHDETECTOR_H

#include <QtCore\QObject>
#include <QtCore\QSharedPointer>
#include <QtCore\QMap>

class QAbstractItemModel;
class Task;

class GlitchDetector : public QObject
{
	Q_OBJECT
public:
	enum Column {
		FilePath,
		AudioLength,
		GlitchCount,
		Progress,
		Column_END
	};

	typedef int TaskID;

	enum { TaskIdRole = Qt::UserRole };

public:
	GlitchDetector(QObject *parent = 0);
	~GlitchDetector();

	TaskID addFile(const QString &path);

	bool startTask(const TaskID &id);

	QAbstractItemModel * taskModel() const { return m_taskModel; }

	QAbstractItemModel * glitchModel(const TaskID &id) const;

	TaskID taskID(int nIndex) const;

signals:
	void error(const QString &);

public slots:
	void setThreshold(double th);
	void abort();
	void reset();
	void rescan();

private slots:
	void updateGlitchCount(int newCount);
	void updateTaskStatus(int status);
	void updateTaskProgress(float progress);

private:
	int senderTaskRow() const;
	int findTaskRow(const TaskID &taskID) const;

private:
	QMap<TaskID, QSharedPointer<Task> > m_mapTasks;
	double threshold;
	QAbstractItemModel * m_taskModel;	
};

#endif // GLITCHDETECTOR_H
