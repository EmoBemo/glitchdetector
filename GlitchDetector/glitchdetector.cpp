#include "glitchdetector.h"
#include <QtGui\QStandardItemModel>
#include <QtCore\QUuid>
#include <QtCore\QDateTime>
#include "detectorengine.h"
#include "filetask.h"
#include "audiofile.h"

GlitchDetector::GlitchDetector(QObject *parent)
	: QObject(parent)
	, threshold(0.1)
{
	m_taskModel = new QStandardItemModel(0, Column_END, this);
	m_taskModel->setHeaderData(FilePath, Qt::Horizontal, tr("Path"));
	m_taskModel->setHeaderData(AudioLength, Qt::Horizontal, tr("Length"));
	m_taskModel->setHeaderData(GlitchCount, Qt::Horizontal, tr("Glitch count"));
	m_taskModel->setHeaderData(Progress, Qt::Horizontal, tr("Progress"));
}

GlitchDetector::~GlitchDetector()
{

}

GlitchDetector::TaskID GlitchDetector::addFile( const QString &path )
{
	// Create new task for the file.
	QSharedPointer<FileTask> task(new FileTask(path));

	// Insert row in the task model.
	const TaskID &taskID = task->id();
	const int row = m_taskModel->rowCount();
	m_taskModel->insertRow(row);
	m_taskModel->setData(m_taskModel->index(row, FilePath), path);
	m_taskModel->setData(m_taskModel->index(row, FilePath), taskID, TaskIdRole);

	// Obtain the audio length.
	AudioFile file(path);
	if (file.open(QIODevice::ReadOnly))
	{
		QTime tm(0, 0);
		tm = tm.addMSecs(file.audioLength());
		m_taskModel->setData(m_taskModel->index(row, AudioLength), tm.toString("hh:mm:ss.zzz"));
	}

	m_mapTasks.insert(taskID, task);

	return taskID;
}

void GlitchDetector::reset()
{
	abort();
	m_taskModel->removeRows(0, m_taskModel->rowCount());
	m_mapTasks.clear();
}

void GlitchDetector::rescan()
{
	QMapIterator<TaskID, QSharedPointer<Task> > It(m_mapTasks);
	while (It.hasNext())
	{
		It.next();
		// Process only tasks that are not currently processed.
		if (It.value()->status() != Task::Processing)
			startTask(It.key());
	}
}

void GlitchDetector::abort()
{
	QList<DetectorEngine*> engines = findChildren<DetectorEngine*>(QString(), Qt::FindDirectChildrenOnly);
	foreach (DetectorEngine* engine, engines)
		engine->abort();
}

int GlitchDetector::senderTaskRow() const
{
	DetectorEngine * engine = static_cast<DetectorEngine*> (sender());
	if (!engine)
		return -1;

	const int taskID = engine->taskID();
	if (taskID < 0)
		return -1;

	return findTaskRow(taskID);
}

int GlitchDetector::findTaskRow( const TaskID &taskID ) const
{
	const QList<QModelIndex> &listMatched = 
		m_taskModel->match(m_taskModel->index(FilePath, 0), TaskIdRole, taskID, 1, Qt::MatchExactly);

	if (listMatched.isEmpty())
		return -1;

	return listMatched.front().row();
}

void GlitchDetector::updateGlitchCount( int newCount)
{	
	const int row = senderTaskRow();
	if (row < 0)
		return;

	m_taskModel->setData(m_taskModel->index(row, GlitchCount), newCount);
}

void GlitchDetector::updateTaskStatus(int status)
{
	const int row = senderTaskRow();
	if (row < 0)
		return;

	if (status == Task::Error)
		m_taskModel->setData(m_taskModel->index(row, Progress), tr("Error"));
	else if (status == Task::Aborted)
		m_taskModel->setData(m_taskModel->index(row, Progress), tr("Aborted"));
}

void GlitchDetector::updateTaskProgress( float progress )
{
	const int row = senderTaskRow();
	if (row < 0)
		return;

	m_taskModel->setData(m_taskModel->index(row, Progress), progress);
}

void GlitchDetector::setThreshold( double th )
{
	threshold = th;
}

bool GlitchDetector::startTask( const TaskID &id )
{
	const SPTask & task = m_mapTasks.value(id);
	if (task.isNull())
		return false;

	DetectorEngine * engine = new DetectorEngine(task, this);
	engine->setThreshold(threshold);
	engine->setAutoDelete(true);

	connect(engine, SIGNAL(glitchCountChanged(int)), this, SLOT(updateGlitchCount(int)));
	connect(engine, SIGNAL(progressChanged(float)), this, SLOT(updateTaskProgress(float)));
	connect(engine, SIGNAL(error(const QString&)), this, SIGNAL(error(const QString&)));
	connect(engine, SIGNAL(taskStatusChanged(int)), this, SLOT(updateTaskStatus(int)));

	return engine->start();
}

QAbstractItemModel *GlitchDetector::glitchModel(const GlitchDetector::TaskID &id) const
{
	const SPTask & task = m_mapTasks.value(id);
	if (task.isNull())
		return 0;

	return task.staticCast<FileTask>()->glitchModel();
}

GlitchDetector::TaskID GlitchDetector::taskID(int nIndex) const
{
	if (nIndex < 0 || nIndex >= m_taskModel->rowCount())
		return -1;

	return m_taskModel->index(nIndex, FilePath).data(TaskIdRole).toInt();
}
