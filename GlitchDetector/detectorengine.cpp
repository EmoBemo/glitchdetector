#include "detectorengine.h"
#include <QtDebug>
#include <typeinfo>
#include "audiofile.h"
#include "filetask.h"

template<typename T, typename U>
struct is_same
{
	static const bool value = false;
};

template<typename T>
struct is_same<T, T>
{
	static const bool value = true;
};

template<typename T, typename U>
bool eqTypes() { return is_same<T, U>::value; }

/////////////////////////////////////////////////////////////////////////////

DetectorEngine::DetectorEngine( const QSharedPointer<Task> &task, QObject *parent)
	: QObject(parent)
	, m_task(task)
	, glitchCount(0)
	, m_threshold(0.5f)
	, m_aborted(false)
	, m_autodelete(false)
{
	qRegisterMetaType<Sample>("Sample");
}

bool DetectorEngine::start()
{
	FileTask * pTask = m_task.dynamicCast<FileTask>().data();
	if (!pTask)
		return false;

	lastProgress = 0.0f;
	glitchCount = 0;
	frameCtr = 0;
	m_aborted = false;
	emit glitchCountChanged(0);
	file.setPath(pTask->path());

	if (!file.open(QIODevice::ReadOnly))
	{
		pTask->setStatus(Task::Error);
		emit taskStatusChanged(pTask->status());
		emit error(tr("Unable to open file %1").arg(pTask->path()));
		file.close();
		if (m_autodelete)
			deleteLater();
		return false;
	}

	pTask->setStatus(Task::Processing);
	emit taskStatusChanged(pTask->status());

	pTask->clearGlitchModel();

	buf.resize(file.channels()*chinkSize);

	QMetaObject::invokeMethod(this, "processChunk", Qt::QueuedConnection, Q_ARG(Sample, 0));

	return true;
}

void DetectorEngine::processChunk(Sample last)
{
	FileTask * task = static_cast<FileTask*> (m_task.data());
	if (m_aborted)
	{
		task->setStatus(Task::Aborted);
		emit taskStatusChanged(m_task->status());
		file.close();
		buf.clear();
		if (m_autodelete)
			deleteLater();
		return;
	}

	// Read the file and find glitches.	
	Sample *tmp = buf.data();
	const int r = file.read(tmp, chinkSize);
	const int channels = file.channels();
	const int frames = r;
	const int samplerate = file.samplerate();
	for (int i = 0; i < frames; ++i)
	{
		const int offset = i*channels;
		const Sample diff = tmp[offset] - last;
		if (qAbs(diff) > m_threshold)
		{
			++glitchCount;
			emit glitchCountChanged(glitchCount);
			const qint64 glitchFrame = frameCtr + i;
			task->addGlitch(glitchFrame, int((1000*glitchFrame)/samplerate/channels), diff);
		}
		last = tmp[offset];
	}
	frameCtr += frames;

	if (r > 0)
	{
		// Send progress changed signal only if there is at least one percent difference.
		const float progress = (float)frameCtr/file.frames();
		if (qRound((lastProgress - progress) * 100))
		{
			emit progressChanged(progress);
			lastProgress = progress;
		}

		// Schedule another processing of another chunk.
		QMetaObject::invokeMethod(this, "processChunk", Qt::QueuedConnection, Q_ARG(Sample, last));
	}
	else
	{
		task->setStatus(Task::Finished);
		emit taskStatusChanged(m_task->status());
		file.close();
		buf.clear();
		if (m_autodelete)
			deleteLater();
	}	
}

void DetectorEngine::setThreshold( float threshold )
{
	if (eqTypes<Sample, float>())
		m_threshold = threshold*2;
	else
		m_threshold = short(threshold*0x7FFF);
}

int DetectorEngine::taskID() const
{
	if (m_task.isNull())
		return -1;

	return m_task->id();
}
