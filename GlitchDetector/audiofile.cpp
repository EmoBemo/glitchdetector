#include "audiofile.h"
#include <QtCore/QFile>

#ifdef Q_OS_WIN
#define LPCWSTR const wchar_t *
#define ENABLE_SNDFILE_WINDOWS_PROTOTYPES 1
#endif

#include "libsndfile/include/sndfile.h"

class AudioFilePrivate
{
public:
	AudioFilePrivate(const QString &name = QString())
	  : fileName(name)
	  , file(0)
	{

	}

	~AudioFilePrivate()
	{
		close();
	}

	bool open(QIODevice::OpenMode mode)
	{
		if (file)
			return false;
#ifdef Q_OS_WIN
		file = sf_wchar_open(fileName.toStdWString().c_str(), mode<<4 & 0x30, &info);
#else
		file = sf_open(fileName.toUtf8().constData(), mode<<4 & 0x30, &info);
#endif
		return file != 0;
	}

	void close()
	{
		if (file)
			sf_close(file);

		memset(&info, 0, sizeof(info));
		file = 0;		
	}

	int read(float * buf, int frames)
	{
		return sf_readf_float(file, buf, frames);
	}

	int read(int * buf, int frames)
	{
		return sf_readf_int(file, buf, frames);
	}

	int read(short * buf, int frames)
	{
		return sf_readf_short(file, buf, frames);
	}

	// members
	QString fileName;
	SNDFILE * file;
	SF_INFO info;
};

//////////////////////////////////////////////////////////////////////////

AudioFile::AudioFile(const QString &name) :
	p(new AudioFilePrivate(name))
{

}

AudioFile::~AudioFile()
{
	delete p;
}

bool AudioFile::open( QIODevice::OpenMode mode )
{
	return p->open(mode);
}

bool AudioFile::isOpen() const
{
	return p->file != 0;
}

void AudioFile::close()
{
	p->close();
}

bool AudioFile::exists()
{
	return QFile::exists(p->fileName);
}

qint64 AudioFile::audioLength() const
{
	return p->info.frames*1000 / p->info.samplerate;
}

int AudioFile::samplerate() const
{
	return p->info.samplerate;
}

int AudioFile::channels() const
{
	return p->info.channels;
}

qint64 AudioFile::frames() const
{
	return p->info.frames;
}

int AudioFile::read( float * buf, int frames )
{
	return p->read(buf, frames);
}

int AudioFile::read(int *buf, int frames)
{
	return p->read(buf, frames);
}

int AudioFile::read(short *buf, int frames)
{
	return p->read(buf, frames);
}

void AudioFile::setPath( const QString &path )
{
	p->fileName = path;
}
