#include "MainWindow.h"
#include <QtWidgets/QApplication>
#include "version.h"
#include "projectdefines.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	a.setApplicationName(PRODUCT_NAME);
	a.setApplicationVersion(STRPRODUCTVER);
	a.setOrganizationName(VENDOR_NAME);
	MainWindow w;
	w.show();
	return a.exec();
}
