#ifndef DETECTORENGINE_H
#define DETECTORENGINE_H

#include <QObject>
#include <QVector>
#include <QtCore\QSharedPointer>
#include <QtCore\QRunnable>
#include "audiofile.h"

class Task;
typedef QSharedPointer<Task> SPTask;

class DetectorEngine : public QObject
{
	Q_OBJECT

	enum { chinkSize = 2048 };
	typedef float Sample;
public:
	DetectorEngine(const SPTask &task, QObject *parent = 0);

	// Sets the threshold to be used.
	void setThreshold(float threshold);

	// Reimplemented
	bool start();

	int taskID() const;

	void setAutoDelete(bool autoDelete = true) { m_autodelete = autoDelete; }
	bool isAutoDelete() const { return m_autodelete; }

signals:
	void glitchCountChanged(int);
	void progressChanged(float);
	void error(const QString &err);
	void taskStatusChanged(int);

public slots:
	void abort() { m_aborted = true; }

private slots:
	void processChunk(Sample last);

private:
	QVector<Sample> buf;
	AudioFile file;
	SPTask m_task;
	int frameCtr;
	int glitchCount;
	float lastProgress;
	Sample m_threshold;
	bool m_aborted;
	bool m_autodelete;
};

#endif // DETECTORENGINE_H
