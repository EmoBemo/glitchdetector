#include "timedelegate.h"
#include <QDateTime>
#include <QPainter>

TimeDelegate::TimeDelegate(QObject *parent) : QItemDelegate(parent)
{

}

void TimeDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QTime time(0, 0);
	time = time.addMSecs(index.data().toInt());

	painter->save();
	painter->setClipRect(option.rect);

	drawBackground(painter, option, index);
	drawDisplay(painter, option, option.rect, time.toString("hh:mm:ss.zzz"));
	drawFocus(painter, option, option.rect);

	painter->restore();
}

