#ifndef TIMEDELEGATE_H
#define TIMEDELEGATE_H

#include <QtWidgets/QItemDelegate>

class TimeDelegate : public QItemDelegate
{
public:
	TimeDelegate(QObject * parent = 0);

	// Reimplemented
	void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // TIMEDELEGATE_H
