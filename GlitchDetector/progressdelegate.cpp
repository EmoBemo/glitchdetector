#include "progressdelegate.h"
#include <QtGui/QPainter>
#include <QtWidgets/QApplication>

ProgressDelegate::ProgressDelegate(QObject *parent) : QStyledItemDelegate(parent)
{

}

void ProgressDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	QStyleOptionProgressBarV2 progressBarOption;

	QRect rect = option.rect;
	QSize size(rect.width()*3/4,rect.height()*3/4);
	rect.setSize(size);

	progressBarOption.state = QStyle::State_Enabled;
	progressBarOption.direction = QApplication::layoutDirection();
	progressBarOption.rect = rect;
	progressBarOption.fontMetrics = QApplication::fontMetrics();

	QPalette pal = progressBarOption.palette;
	QColor col;
	col.setNamedColor("#05B8CC");
	pal.setColor(QPalette::Highlight,col);
	//pal.setColor(QPalette::HighlightedText,Qt::black);
	progressBarOption.palette = pal;

	progressBarOption.type = QStyleOption::SO_ProgressBar;
	progressBarOption.version = 2 ;

	progressBarOption.minimum = 0;
	progressBarOption.maximum = 100;
	progressBarOption.textAlignment = Qt::AlignCenter;
	progressBarOption.textVisible = true;

	const QVariant &data = index.data(Qt::DisplayRole);
	int progress = int(qRound(data.toFloat()*100));
	// Set the progress and text values of the style option.

	progressBarOption.progress = progress < 0 ? 0 : progress;
	progressBarOption.text = data.type() == QVariant::String ? data.toString() :
				QString("%1%").arg(progressBarOption.progress);

	// Draw the progress bar onto the view.
	QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter, 0);
}

