; this file can not be compiled as standalone
; it shoulud be included

#pragma verboselevel 9

#include "..\GlitchDetector\setupdef.h"
                        
; Define Qt directories
#define QT_BIN_DIR QT_BASE_DIR+"bin\"
#define QT_PLUGINS_DIR QT_BASE_DIR+"plugins\"
#define QT_IMAGEFORMATS_DIR QT_PLUGINS_DIR+"imageformats\"
#define QT_PLATFORMS_DIR QT_PLUGINS_DIR+"platforms\"
#define QT_SQLDRIVERS_DIR QT_PLUGINS_DIR+"sqldrivers\"

#define APP_VER_NAME PRODUCT_NAME+" "+STRSHORTVER
#define PRODUCT_DIR PRODUCT_NAME+"\"+STRPRODUCTVER+"\"
#define FULL_OUTPUT_NAME SETUP_BASE_NAME + "-setup"
#define PROJECT_DIR "..\GlitchDetector\"

#define OUTPUT_FILE_PATH PRODUCT_DIR+FULL_OUTPUT_NAME+".exe"
#define EXE_FULL_NAME EXE_NAME+".exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)

AppId={#ApplicationID}
AppName={#PRODUCT_NAME}
AppVerName={#APP_VER_NAME}
AppPublisher={#VENDOR_NAME}
AppPublisherURL={#PRODUCT_URL}
;AppSupportURL={#SUPPORT_URL}
AppUpdatesURL={#PRODUCT_URL}
DefaultDirName={pf}\{#PRODUCT_NAME}
DefaultGroupName={#PRODUCT_NAME}
AllowNoIcons=yes
OutputDir={#PRODUCT_DIR}
OutputBaseFilename={#FULL_OUTPUT_NAME}
SetupIconFile="Setup.ico"
SetupLogging=no
Compression=lzma2/ultra64
SolidCompression=yes
VersionInfoVersion={#STRPRODUCTVER}
WizardImageFile="WizardImageFile.bmp"
WizardSmallImageFile="WizardSmallImageFile.bmp"
;PrivilegesRequired=admin
UninstallDisplayIcon="Setup.ico"

;LicenseFile=EULA.rtf

[Messages]
BeveledLabel={#VENDOR_NAME}

[Files]
Source: "{#EXECUTABLE_PATH}"; DestDir: {app}; Flags: ignoreversion
Source: "{#PROJECT_DIR}libsndfile-1.dll"; DestDir: {app}; Flags: ignoreversion

; Qt files
Source: "{#QT_BIN_DIR}Qt5Core.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#QT_BIN_DIR}Qt5Gui.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#QT_BIN_DIR}Qt5Widgets.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#QT_PLATFORMS_DIR}qwindows.dll"; DestDir: "{app}/platforms"; Flags: ignoreversion
Source: "vcredist_x86.exe"; DestDir: {tmp}; Flags: ignoreversion deleteafterinstall

[Icons]
Name: {group}\{#PRODUCT_NAME}; Filename: {app}\{#EXE_NAME}
Name: {group}\{cm:UninstallProgram,{#PRODUCT_NAME}}; Filename: {uninstallexe}
Name: {commondesktop}\{#PRODUCT_NAME}; Filename: {app}\{#EXE_NAME};

[Run]
Filename: {tmp}\vcredist_x86.exe; Parameters: "/passive /Q:a /c:""msiexec /qb /i vcredist.msi"" "; StatusMsg: "Installing CRT..."; Flags: runhidden; Check: VC2008RedistNeedsInstall
Filename: {app}\{#EXE_NAME}; Description: {cm:LaunchProgram,{#PRODUCT_NAME}}; Flags: waituntilidle postinstall skipifsilent; WorkingDir: "{app}"

#include "vcredist.iss"