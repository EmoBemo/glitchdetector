# README #

This README describes the steps that are necessary to get your application up and running.

### What is this repository for? ###

* This repository hosts the source code and setup binaries of the Glitch Detector application.

### How do I get set up? ###

* Summary of set up
  - The project currently can be build in Qt Creator or VS2008.
  - To build the project with VS2008 the Qt VisualStudio Add-in is required.
* Dependencies
  - Qt5
* Deployment instructions
  - Creating setups with Qt Creator is currently not supported.
  - Inno Setup 5 is required in order to create setup files with VS2008.
  - To create setup file with VS2008 activate Release_Setup configuration and rebuild.


### Contribution guidelines ###

* If you want to contribute to the project write me a message.

### Who do I talk to? ###

* Repo owner or admin